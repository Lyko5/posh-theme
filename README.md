# PowerShell ISE Theme

Colors copied from VSCode OneDark-Pro theme by [Binaryify](https://github.com/binaryify/onedark-pro)

## Script and Output Pane

![Script Pane](images/scriptPane.png "Script Pane")

## Output Streams

![Output Streams](images/outputStreams.png "Output Streams")
